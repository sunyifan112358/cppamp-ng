//===----- CGCUDARuntime.h - Interface to CUDA Runtimes ---------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This provides an abstract class for CUDA code generation.  Concrete
// subclasses of this implement code generation for specific CUDA
// runtime libraries.
//
//===----------------------------------------------------------------------===//

#ifndef CLANG_CODEGEN_AMPRUNTIME_H
#define CLANG_CODEGEN_AMPRUNTIME_H

namespace clang {

namespace CodeGen {

class CodeGenFunction;
class CodeGenModule;
class FunctionArgList;
class ReturnValueSlot;
class RValue;

class CGAMPRuntime {
protected:
  CodeGenModule &CGM;

public:
  CGAMPRuntime(CodeGenModule &CGM) : CGM(CGM) {}
  virtual ~CGAMPRuntime();
  virtual void EmitTrampolineBody(CodeGenFunction &CGF, const FunctionDecl *FD,
                                  FunctionArgList &Args);
  void EmitTrampolineNameBody(CodeGenFunction &CGF, const FunctionDecl *FD,
    FunctionArgList &Args);
};

/// Creates an instance of a CUDA runtime class.
CGAMPRuntime *CreateAMPRuntime(CodeGenModule &CGM);

}
}

#endif
